const{test, request, expect} = require('@playwright/test')
const{LoginPage} = require('../PageObjects/LoginPage')
const{DashboardPage} = require('../PageObjects/DashboardPage')
const payLoad = {userEmail:"akenya@gmail.com",userPassword:"Ghaja@321"}

test.beforeAll( async()=> {
    const apiContext =  await request.newContext();
    const loginResponse = await apiContext.post("https://rahulshettyacademy.com/client",{
        data:payLoad
    })
    expect(loginResponse.ok()).toBeTruthy();
    const loginResponseInJson = loginResponse.json();
    const token = loginResponseInJson.token;
    console.log(token);
});

test.only("Add a product to the cart", async({page}) => {
    // const username = "akenya@gmail.com";
    // const password = "Ghaja@321";
    const productName = "IPHONE 13 PRO";
    const loginpage = new LoginPage();
    await loginpage.goToClientPage();
    await loginpage.validLogin(username, password);
    const dashboardPage = new DashboardPage();
    await dashboardPage.searchProduct(productName);
    await dashboardPage.verifyAddedProduct(productName);
});