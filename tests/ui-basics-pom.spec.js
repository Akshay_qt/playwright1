import{test, expect} from "@playwright/test"
const{LoginPage} = require('../PageObjects/LoginPage')
const{DashboardPage} = require('../PageObjects/DashboardPage')

test('Invalid Login verification text', async({page}) => {
    const username = "akenya@gmail.com";
    const password = "Ghaja@321";
    const loginpage = new LoginPage(page);
    await loginpage.goTo();
    await loginpage.inValidLogin(username, password);
    await expect(loginpage.errorText).toContainText('Incorrect');
});

test("Add a product to the cart", async({page}) => {
    const username = "akenya@gmail.com";
    const password = "Ghaja@321";
    const productName = "IPHONE 13 PRO";
    const loginpage = new LoginPage();
    await loginpage.goToClientPage();
    await loginpage.validLogin(username, password);
    const dashboardPage = new DashboardPage();
    await dashboardPage.searchProduct(productName);
    await dashboardPage.verifyAddedProduct(productName);
});

test("handling child windows", async({browser}) => {
    const newOne =  await browser.newContext();
    const page =  await newOne.newPage();
    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    const documentLink = page.locator('[href*="documents-request"]');
    const [newPage1] = await Promise.all([
        newOne.waitForEvent('page'),
        documentLink.click(),
    ]);
    const text = await newPage1.locator('.red').textContent();
    console.log('text' + text);
    const splittedText = text.split("@")
    let username = splittedText[1].split(" ")[0];
    await page.locator('#username').type(username);
    await page.locator('input[type="password"]').fill('learning');
 });

 test("Handling the static dropdown", async({page}) => {
    const email = "rahulshetty";
    const password = "Ghaja@321";
    // const expectedTitles = [ 'ZARA COAT 3', 'ADIDAS ORIGINAL', 'IPHONE 13 PRO' ];
    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    await page.locator('#username').type(email);
    await page.locator('input[type="password"]').fill(password);
    await page.locator('select.form-control').selectOption('Teacher');
    await page.locator('#usertype').nth(1).click();
    await page.locator('#okayBtn').click();
    await expect(page.locator('#usertype').nth(1)).toBeChecked();
    const value = await page.locator('#usertype').nth(1).isChecked();
    expect(value).toBeTruthy();
    await expect(page.locator('[href*="documents-request"]')).toHaveAttribute("class", 'blinkingText');
    // await page.pause();
 });
