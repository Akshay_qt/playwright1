import { expect } from "@playwright/test";
class DashboardPage {
    constructor(page) {
        this.page = page;
        this.productNames = page.locator("div.card-body");
        this.productText = page.locator("div.card-body b");
        this.cartButton = page.locator('[routerlink="/dashboard/cart"]');
        this.cartSection = page.locator('div.infoWrap');
    }
    async searchProduct(productName) {
        const count = await this.productNames.count();
        console.log('no of products' +count);
        // add to cart
        for (let i = 0; i < count; ++i) {
            if (await this.productNames.nth(i).locator("b").textContent() === productName) {
                await this.productNames.nth(i).locator("text=Add To Cart").click();
                break;
            }
        }
        await this.cartButton.click();
    }
    async verifyAddedProduct(productName) {
        await expect(this.cartSection.locator('div h3')).toHaveText(productName);
        console.log(await this.cartSection.locator('div h3').textContent());
    }


}
module.exports = {DashboardPage}