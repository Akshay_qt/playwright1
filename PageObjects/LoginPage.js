class LoginPage{
    constructor(page){
    this.page = page;
    this.username =  page.locator('#username');
    this.password =  page.locator('input[type="password"]');
    this.signInButton =  page.locator('input[name="signin"]');
    this.errorText = page.locator('.alert.alert-danger');
    this.email = page.locator("#userEmail");
    this.password1 = page.locator("#userPassword");
    this.login = page.locator("[value='Login']");
    }

    async goTo() {
        await this.page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    }

    async inValidLogin(username, password) {
        await this.username.fill(username);
        await this.password.fill(password);
        await this.signInButton.click();
        await this.page.waitForLoadState('networkidle');
    }
    async goToClientPage() {
        await this.page.goto("https://rahulshettyacademy.com/client");
    }

    async validLogin(username, password) {
        await this.email.fill(username);
        await  this.password1.fill(password);
        await this.login.click();
        await this.page.waitForLoadState('networkidle');
    }
    

}
module.exports = {LoginPage}